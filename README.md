Untuk menjalan docker berikut merupakan step-step nya :
	1. Gunakan command "docker build -t html-hits:v1 . "
	2. cek keberhasilan pembuatan image
	3. Jalankan aplikasi dengan menggunakan command "docker run -d -p 80:80 html-hits:v1" (Service berjalan di port 80)
	4. Untuk keperluan scale up, dapat menggunakan perintah "docker service scale [Service Name]=[Jumlah replika]"
